ctx_with_test:
  cmd.run:
    - cmd: "echo test1"

ctx_with_test_ignore_true:
  cmd.run:
    - cmd: "echo test2"
    - ignore_test: True

ctx_with_test_ignore_false:
  cmd.run:
    - cmd: "echo test3"
    - ignore_test: false
